FROM google/cloud-sdk:alpine AS base

RUN apk update
RUN apk add --update npm
RUN sudo chown -R 65534:0 "/root/.npm"
RUN npm install -g firebase-tools
